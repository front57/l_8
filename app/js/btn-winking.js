function winking(e){
    if (!e.parentElement.classList.contains('animated') && !e.classList.contains('slick-disabled')){
        e.parentElement.classList.add('animated');
        document.querySelector('.animated').addEventListener('animationend', function(){
            e.parentElement.classList.remove('animated');
        });
    }
}




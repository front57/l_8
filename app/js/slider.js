$('.reviews__slider').slick({
    infinite: false,
    prevArrow: false,
    nextArrow: false,
    dots: true,
    variableWidth:true,
    slidesToShow: 1,
    slidesToScroll: 1,
    appendDots: $('.reviews__slick-dots'),
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 768,
            settings: {
                variableWidth:false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

$('.results__slider').slick({
    prevArrow: document.querySelector('.results__slider-prev'),
    nextArrow: document.querySelector('.results__slider-next'),
    dots:false,
    slidesToShow:1,
    slidesToScroll:1,
    infinite: false,
})
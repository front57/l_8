let prevValue = document.querySelector('input[name="price-radio"]:checked').value;

function radioUpd(e){
    if( e.value !== prevValue){
        updPlans(parseInt(e.value));
    }
    prevValue = e.value;
}

function updPlans(val){
    const prices = document.querySelectorAll('.price__card-cost');
    if (val > 1){
        prices.forEach(e => {
            e.innerText ='$' + parseInt(e.innerText.slice(1)) * val;
        });
    }else{
        prices.forEach(e => {
            e.innerText = '$' + parseInt(e.innerText.slice(1)) / 11;
        });
    }
}
